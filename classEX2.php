<?php
class htmlpage {
    protected $title ="EX";
    protected $body="This is my exercise ";
    public function view(){
        echo"<html>
            <head>
            <title>$this->title</title>
            </head>
            <body><b><u>$this->body</u></b></body>
        </html>";
            }
    function __construct ($title="",$body=""){
         if($title!=""){
            $this->title=$title;
            }
        if($body!=""){
             $this->body=$body;

}  
}
}
class colortext extends htmlpage {
    protected $color='pink';
    public function __set($property,$value){
        if($property =='color'){
            $colors= array ('purple','black','green','pink','yellow','red') ;
            if(in_array($value,$colors)) {
            $this->color= $value;
            }
            else { 
            die("The text color is not in the array color , the colors in the array are:pink,purple,green,yellow,red and black");

        }
}

    }
 public function view(){ echo"
  
    <p style ='color:$this->color'><b><u>$this->body</b></u></p>";
  }
}

class fontsize extends colortext {
    protected $size='12';
    public function __set($property,$value){
        colortext::__set($property,$value);
        if($property =='size'){
            $sizes=range(10,24);
            if(in_array($value,$sizes)){
            $this->size= $value;
            }
            else { 
            die("Invalid entry!!<br>
            Valid colors:pink,purple,green,yellow,red and black<br>
            text size it must be verified to be an integer between 10 and 24");
            
        }}
}
public function view(){ echo"
  
    <p style ='font-size:$this->size; color:$this->color'> <b><u>$this->body</b></u></p>";
  }

    }

?>
